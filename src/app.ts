import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import { PORT, MONGO_URL } from "./config/environment";
import { userRouter } from "./router/userRouter";
import { errorHandler, successHandler } from "./config/morgan";
import logger from "./config/logger";

const app = express();
app.use(bodyParser.json());

app.use(successHandler);
app.use(errorHandler);

mongoose
  .connect(MONGO_URL)
  .then(() => {
    logger.info("Connected to MongoDB");

    app.listen(PORT, () => {
      logger.info(`Listening to port ${PORT}`);
    });

    app.use("/v1/users", userRouter);
  })
  .catch(() => {
    logger.error("Failed to connect to MongoDB");
  });

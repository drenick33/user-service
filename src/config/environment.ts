import dotenv from "dotenv";

dotenv.config();

/**
 * The port for the server to run on
 */
export const PORT: number = Number(process.env.PORT) || 3000;

export const MONGO_URL: string = process.env.MONGO_URL || "";

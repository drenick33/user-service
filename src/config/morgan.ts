import { Request, Response } from "express";
import morgan from "morgan";
import logger from "./logger";

const successResponseFormat = `:date[web]
  :method :url :status - :response-time ms`;
const errorResponseFormat = `:date[web] :method :url :status - :response-time ms`;

export const successHandler = morgan(successResponseFormat, {
  skip: (req: Request, res: Response) => res.statusCode >= 400,
  stream: {
    write: (message: string) => {
      logger.info(message.trim());
    },
  },
});

export const errorHandler = morgan(errorResponseFormat, {
  skip: (req: Request, res: Response) => res.statusCode < 400,
  stream: { write: (message: string) => logger.error(message.trim()) },
});

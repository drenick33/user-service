import { Router } from "express";
import { createUserController } from "../controllers/createUser";
import { deleteUserByIdController } from "../controllers/deleteUserById";
import { getUserByIdController } from "../controllers/getUserById";
import { listUsersController } from "../controllers/listUsers";
import { updateUserByIdController } from "../controllers/updateUserById";

export const router = Router();

router.route("/").post(createUserController).get(listUsersController);

router
  .route("/:userId")
  .get(getUserByIdController)
  .patch(updateUserByIdController)
  .delete(deleteUserByIdController);

export { router as userRouter };

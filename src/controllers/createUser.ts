import { Request } from "express";
import httpStatus from "http-status";
import { UserInterface } from "../model/user";
import { createUser } from "../services/createUser";
import { catchAsync } from "../utils/catchAsync";

export const createUserController = catchAsync(
  async (req: Request<{}, {}, UserInterface>, res) => {
    const user = await createUser(req.body);
    res.status(httpStatus.CREATED).send(user);
  }
);

import { listUsers } from "../services/listUsers";
import { catchAsync } from "../utils/catchAsync";

export const listUsersController = catchAsync(async (req, res) => {
  const users = await listUsers();
  res.send(users);
});

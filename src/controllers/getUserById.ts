import { Request, Response } from "express";
import httpStatus from "http-status";
import { getUserById } from "../services/getUserById";
import { catchAsync } from "../utils/catchAsync";

interface UserParams {
  userId: string;
}

export const getUserByIdController = catchAsync(
  async (req: Request<UserParams>, res: Response) => {
    const { userId } = req.params;
    const user = await getUserById(userId);

    if (!user) {
      return res.sendStatus(httpStatus.NOT_FOUND);
    }

    return res.send(user);
  }
);

import { Request } from "express";
import httpStatus from "http-status";
import { UpdateQuery } from "mongoose";
import { UserDocument } from "../model/user";
import { updateUserById } from "../services/updateUserById";
import { catchAsync } from "../utils/catchAsync";

interface UserParams {
  userId: string;
}

type UserUpdate = UpdateQuery<UserDocument>;

export const updateUserByIdController = catchAsync(
  async (req: Request<UserParams, {}, UserUpdate>, res) => {
    const { userId } = req.params;
    const updateBody = req.body;
    const user = await updateUserById({ userId, updateBody });

    if (!user) {
      res.status(httpStatus.NOT_FOUND).send("User Not Found");
    }

    res.send(user);
  }
);

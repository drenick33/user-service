import { Request } from "express";
import httpStatus from "http-status";
import { deleteUserById } from "../services/deleteUserById";
import { catchAsync } from "../utils/catchAsync";

interface UserParams {
  userId: string;
}

export const deleteUserByIdController = catchAsync(
  async (req: Request<UserParams>, res) => {
    const { userId } = req.params;
    const deletedUser = await deleteUserById(userId);

    if (!deletedUser) {
      return res.sendStatus(httpStatus.NOT_FOUND);
    }

    res.sendStatus(httpStatus.NO_CONTENT);
  }
);

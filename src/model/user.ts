import { Schema, model, Document } from "mongoose";
import { v4 as uuid } from "uuid";

enum Role {
  USER = "user",
  BLOCKED = "blocked",
  ADMIN = "admin",
}

export interface UserInterface {
  username: string;
  firstName?: string;
  lastName?: string;
  email: string;
  password: string;
  role: Role;
  PID: string;
  createdAt: Date;
}

export interface UserDocument extends Document, UserInterface {}

const userSchema = new Schema({
  username: {
    type: String,
    required: true,
  },
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    require: true,
    enum: Object.values(Role),
    default: Role.USER,
  },
  PID: {
    type: String,
    require: true,
    default: () => uuid(),
  },
  createdAt: {
    type: Date,
    default: () => new Date(),
  },
});

const User = model<UserDocument>("User", userSchema);

export default User;

import User, { UserInterface } from "../model/user";

export const createUser = async (user: UserInterface) =>
  await new User(user).save();

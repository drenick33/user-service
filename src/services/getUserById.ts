import User from "../model/user";

export const getUserById = async (userId: string) =>
  await User.findById(userId);

import User from "../model/user";

export const deleteUserById = async (userId: string) =>
  await User.findByIdAndDelete(userId);

import { UpdateQuery } from "mongoose";
import User, { UserDocument } from "../model/user";

interface UpdateUserByIdProps {
  userId: string;
  updateBody: UpdateQuery<UserDocument>;
}

export const updateUserById = async ({
  userId,
  updateBody,
}: UpdateUserByIdProps) =>
  await User.findByIdAndUpdate(userId, updateBody, { new: true });

import User from "../model/user";

export const listUsers = async () => await User.find({});
